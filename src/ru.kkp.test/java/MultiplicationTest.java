import static org.junit.Assert.*;

public class MultiplicationTest {

    @org.junit.Test
    public void negative() {
        int num1 = -50 + (int) (Math.random() * 49);
        int num2 = -50 + (int) (Math.random() * 49);
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }
    @org.junit.Test
    public void positive() {
        int num1 = 1 + (int) (Math.random() * 200);
        int num2 = 1 + (int) (Math.random() * 100);
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }

    @org.junit.Test
    public void zero() {
        int num1 = 0;
        int num2 = 0;
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }
    @org.junit.Test
    public void zero1() {
        int num1 = 1;
        int num2 = 0;
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }
    @org.junit.Test
    public void zero2() {
        int num1 = 0;
        int num2 = 1;
        assertEquals(num1 * num2, Multiplication.multiply(num1, num2));
    }

}